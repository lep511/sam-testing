target:
	$(info ${HELP_MESSAGE})
	@exit 0

clean: ##=> Deletes current build environment and latest build
	$(info [*] Who needs all that anyway? Destroying environment....)
	rm -rf ./.aws-sam/

checkOSDependencies:
	python3 --version || grep "3.9" || (echo "Error: Requires Python 3.9" && exit 1)

s3: 
	$(info [*] Check if artifact bucket exist....)
	if aws s3api head-bucket --bucket $AWS_BUCKET 2>/dev/null; then echo "Bucket $AWS_BUCKET exist."; else aws s3 mb s3://${AWS_BUCKET} --region ${AWS_REGION} ; fi

deps:
	pip install -r test/requirements.txt

coverage:
	coverage run -m unittest discover
	coverage xml --omit "test/*"

test:
	pytest -v --disable-socket -s test/unit/src/

scan:
	sam validate --lint
	pylint src/sample_lambda/*.py test/unit/src/*.py --errors-only --disable=import-error,no-value-for-parameter 

build:
	$(info [+] Installing '3rd party' dependencies...)
	python -m pip install -r requirements.txt -t src/sample_lambda
	rm -rf src/sample_lambda/bin